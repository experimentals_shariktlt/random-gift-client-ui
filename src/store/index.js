import Vue from 'vue'
import Vuex from 'vuex'

import user from './user'
import page from './page'
import market from './market'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    user,
    page,
    market
  }
})

export default store
