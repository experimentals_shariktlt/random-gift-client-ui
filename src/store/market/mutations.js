/*
export const someMutation = (state) => {}
 */
export const startLoadAvailable = (state) => {
  state.loadAvailable = true
  state.availablePacks.error = false
}

export const endSuccessLoadAvailable = (state, list) => {
  state.loadAvailable = false
  state.availablePacks.error = false
  state.availablePacks.list = list
  state.availablePacks.loaded = true
}

export const endErrorLoadAvailable = (state, msg) => {
  state.loadAvailable = false
  state.availablePacks.error = msg
}

export const startLoadUser = (state) => {
  state.loadUserPacks = true
  state.errorLoadAvailable = false
}

export const endSuccessLoadUser = (state, list) => {
  state.loadUserPacks = false
  state.userPacks.error = false
  state.userPacks.list = list
  state.userPacks.loaded = true
}

export const endErrorLoadUser = (state, msg) => {
  state.loadUserPacks = false
  state.userPacks.error = msg
}

export const addpack = (state, pack) => {
  console.log('addpack ', state.userPacks.list)
  state.userPacks.list.push(pack)
}

export const addpacks = (state, packs) => {
  for (let i = 0, len = packs.length; i < len; i++) {
    state.userPacks.list.push(packs[i])
  }
}

export const removepack = (state, packId) => {
  let key = state.userPacks.list.findIndex((item, index, array) => item.id === packId)
  state.userPacks.list.splice(key, 1)
}
export const updatepack = (state, pack) => {
  let key = state.userPacks.list.findIndex((item, index, array) => item.id === pack.id)
  // state.userPacks.list.splice(key, 1)
  // state.userPacks.list.unshift(pack)
  state.userPacks.list[key] = pack
  state.userPacks.list.splice()
}
