/*
export const someAction = (state) => {}
 */
import axios from 'axios'
import { Dialog, Notify } from 'quasar'
export const loadAvailable = ({state, commit}) => {
  if (state.loadAvailable) {
    return null
  }
  commit('startLoadAvailable')
  axios.get('/api/v1/public/packs/list')
    .then((response) => {
      if (response.data.success) {
        commit('endSuccessLoadAvailable', response.data.body.packs)
      } else {
        console.log(response.data.body)
        commit('endErrorLoadAvailable', 'Попробуйте позже')
      }
    }).catch((e) => {
      let msg = 'Ошибка загрузки списка наборов'
      commit('endErrorLoadAvailable', msg)
    })
}

export const buy = ({state, commit, dispatch}, buyInfo) => {
  axios.post('/api/v1/private/packs/user/buy', {packId: buyInfo.packId, count: buyInfo.count})
    .then(response => {
      console.log(response)
      if (response.data.success) {
        var pack = response.data.body[0].pack
        commit('addpacks', response.data.body)
        commit('user/balance', response.data.balance, { root: true })
        Notify.create({
          message: 'Куплено ' + pack.name + ' x' + response.data.body.length,
          timeout: 1000,
          type: 'positive'
        })
      }
    })
    .catch(e => {
      console.log(e)
      Notify.create({
        message: 'Невозможно купить сейчас, попробуйте позже',
        timeout: 2000
      })
    })
}

export const sell = ({state, commit, dispatch}, packId) => {
  axios.post('/api/v1/private/packs/user/sell', {packId})
    .then(response => {
      console.log(response)
      if (response.data.success) {
        Notify.create({
          message: 'Продано',
          type: 'positive',
          timeout: 1000
        })
        commit('removepack', packId)
        commit('user/balance', response.data.balance, { root: true })
      }
    })
    .catch(e => {
      console.log(e)
      Notify.create({
        message: 'Невозможно продать сейчас, попробуйте позже',
        timeout: 2000
      })
    })
}

export const unwrap = ({state, commit, dispatch}, packId) => {
  axios.post('/api/v1/private/packs/user/unwrap', {packId})
    .then(response => {
      console.log(response)
      if (response.data.success) {
        let pack = response.data.body
        Notify.create({
          message: 'Открыто ' + pack.name + ' ' + pack.price,
          type: 'positive',
          timeout: 1000
        })
        commit('updatepack', pack)
        commit('user/balance', response.data.balance, { root: true })
      }
    })
    .catch(e => {
      console.log(e)
      Dialog.create({
        title: 'Ошибка',
        message: 'Невозможно продать сейчас, попробуйте позже'
      })
    })
}

export const loadUserPacks = ({state, commit, dispatch}) => {
  if (state.loadAvailable) {
    return null
  }
  commit('startLoadUser')
  axios.get('/api/v1/private/packs/user/list')
    .then((response) => {
      if (response.data.success) {
        commit('endSuccessLoadUser', response.data.body)
      } else {
        console.log(response.data.body)
        commit('endErrorLoadUser', 'Попробуйте позже')
      }
    }).catch((e) => {
      let msg = 'Ошибка загрузки списка наборов'
      commit('endErrorLoadUser', msg)
    })
}
