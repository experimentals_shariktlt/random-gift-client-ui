module.exports = {
  computed: {
    profile () {
      return this.$store.state.user.profileData
    },
    logged () {
      return this.$store.state.user.logged
    },
    user () {
      return this.$store.state.user
    }
  },
  methods: {
    checkLogged () {
      if (this.$store.state.user.initial) {
        return null
      }
      if (!this.logged) {
        console.log('redirect to main', this.logged)
        this.$router.push('/')
      }
    }
  },
  watch: {
    initiated (val, old) {
      console.log('Initiated watch')
      this.checkLogged()
    },
    logged (val, old) {
      console.log('logged watch', val, old)
      this.checkLogged()
    }
  },
  created () {
    console.log('user mixin created')
    this.checkLogged()
  }
}
