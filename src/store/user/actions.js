import axios from 'axios'

export const register = ({commit, state, dispatch}, registerProps) => {
  if (state.sendRegister) {
    return null
  }
  commit('sendRegister', true)
  commit('registerError', false)
  axios.post('/api/v1/public/user/register', {
    login: registerProps.login,
    email: registerProps.login,
    password: registerProps.password
  })
    .then(response => {
      console.log(response)
      if (response.data.success) {
        if (!state.sendLogin) {
          // TODO REFACTOR TO AUTOLOGIN AFTER SIGNUP
          dispatch('login', registerProps)
        }
      } else {
        if (response.data.body === 'user exist') {
          commit('registerError', 'Пользователь с таким e-mail уже существует')
        }
      }
      commit('sendRegister', false)
    })
    .catch(e => {
      console.error(e)
      let msg = 'Неизвестная ошибка, попробуйте позже'
      // switch (e.response.data.body) {
      //   case 'user not found':
      //     msg = 'Неправильный e-mail или пароль'
      //     break
      // }
      commit('registerError', msg)
      commit('sendRegister', false)
    })
}

export const login = ({commit, dispatch, state}, loginProps) => {
  if (state.sendLogin) {
    return null
  }
  commit('sendLogin', true)
  commit('loginError', false)
  axios.post('/api/v1/public/user/login', {
    login: loginProps.login,
    password: loginProps.password
  })
    .then(response => {
      console.log(response)
      if (response.data.success) {
        dispatch('checkProfile')
      } else {
        console.log(response.data)
      }
      commit('sendLogin', false)
    })
    .catch(e => {
      console.error(e)
      let msg = 'Неизвестная ошибка, попробуйте позже'
      switch (e.response.data.body) {
        case 'user not found':
          msg = 'Неправильный e-mail или пароль'
          break
      }
      commit('loginError', msg)
      commit('sendLogin', false)
    })
}

export const checkProfile = ({commit, state}) => {
  if (state.sendCheck) {
    console.log('sendCheck is true')
    return null
  }
  commit('sendCheck', true)
  axios.get('/api/v1/private/user/check')
    .then(response => {
      if (response.data.success) {
        commit('updateProfile', response.data.body)
        commit('logged', true)
      }
      commit('sendCheck', false)
      if (state.initial) {
        commit('initiated')
      }
    }).catch(e => {
      commit('logged', false)
      commit('sendCheck', false)
    // TODO emit some error to user
    })
}

export const check = ({commit, state, dispatch}) => {
  if (!state.initial) {
    return null
  }
  dispatch('checkProfile')
}

export const logout = ({commit, state}) => {
  if (state.sendLogout) {
    return null
  }
  commit('sendLogout', true)

  axios.post('/api/v1/private/user/logout')
    .then(response => {
      if (response.data) {
        commit('logged', false)
      }
      commit('sendLogout', false)
    })
    .catch(e => {
      commit('sendLogout', false)
    })
}

export const promoActivate = ({commit, state}, code) => {
  if (state.sendPromoActivate) {
    return null
  }
  commit('sendPromoActivate', true)
  axios.post('/api/v1/private/wallet/promo', {code: code})
    .then(response => {
      console.log(response.data)
      if (response.data.success) {
        console.log('Promo ok')
        commit('balance', response.data.balance)
      }
      commit('sendPromoActivate', false)
    })
    .catch(e => {
      commit('sendPromoActivate', false)
    })
}
