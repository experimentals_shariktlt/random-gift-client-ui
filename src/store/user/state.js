/* eslint-disable no-undef */
export default {
  userId: null,
  login: null,
  balance: null,
  logged: false,
  sendLogin: false,
  loginError: false,
  registerError: false,
  initial: true
}
