export const logged = (state, val) => {
  state.logged = val === true
}

export const logout = (state) => {
  state.logged = false
}

export const sendLogin = (state, prop) => {
  state.sendLogin = prop === true
}

export const sendLogout = (state, prop) => {
  state.sendLogout = prop === true
}

export const loginError = (state, prop) => {
  state.loginError = prop
}

export const registerError = (state, prop) => {
  state.registerError = prop
}

export const sendRegister = (state, prop) => {
  state.sendRegister = prop === true
}

export const sendCheck = (state, prop) => {
  state.sendCheck = prop === true
}

export const initiated = (state) => {
  state.initial = false
}

export const updateProfile = (state, profileData) => {
  state.profileData = profileData
  state.balance = state.profileData.balance
  console.log('profileData updated')
}

export const noop = (state) => {
  console.log('no op')
}

export const sendPromoActivate = (state, prop) => {
  state.sendPromoActivate = prop === true
}

export const balance = (state, val) => {
  console.log('start balance by ', val)
  let amount = parseInt(val, 10)
  console.log('parsed ' + amount)
  if (amount >= 0 && state.profileData !== undefined) {
    try {
      state.profileData = {...state.profileData, balance: amount}
      state.balance = amount
    } catch (e) {
      console.error(e)
    }
    console.log('balance updated ' + amount)
  } else {
    console.log('cant update balance')
  }
}
