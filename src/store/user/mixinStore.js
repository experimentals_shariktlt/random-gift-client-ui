module.exports = {
  computed: {
    user () {
      return this.$store.state.user
    },
    profile () {
      return this.$store.state.user.profileData
    },
    logged () {
      return this.$store.state.user.logged
    }
  }
}
