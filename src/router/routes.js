
export default [
  {
    path: '/',
    component: () => import('layouts/alpha'),
    children: [
      { path: '', component: () => import('pages/index') }
    ]
  },
  {
    path: '/login',
    component: () => import('layouts/alpha'),
    children: [
      {path: '', component: () => import('pages/public/user/login')}
    ]
  },
  {
    path: '/register',
    component: () => import('layouts/alpha'),
    children: [
      {path: '', component: () => import('pages/public/user/register')}
    ]
  },
  {
    path: '/logout',
    component: () => import('layouts/alpha'),
    children: [
      {path: '', component: () => import('pages/private/user/logout')}
    ]
  },
  {
    path: '/deposit',
    component: () => import('layouts/alpha'),
    children: [
      {path: '', component: () => import('pages/private/user/deposit')},
      {path: 'promo', component: () => import('pages/private/user/deposit/promo')}
    ]
  },
  {
    path: '/inventory',
    component: () => import('layouts/alpha'),
    children: [
      {path: '', component: () => import('pages/private/user/inventory')}
    ]
  },
  { // Always leave this as last one
    path: '*',
    component: () => import('pages/404')
  }
]
